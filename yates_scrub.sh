#!/bin/bash
# To be used inside the yates folder
# USAGE: ./yates_script <*.dot> <matrixfile> <*.hosts> <scrubfile> <attack_strength>
# Scrubfile is the file that determines the real throughput in a network.
# This script uses scrubber_simulation.py to assess attack results in the network. The matrix is then
# put through yates to determine congestion. Congestion files are also saved and compiled into 
# a csv file. 

if [ $# -eq 7 ]
then
	if [ $6	!= "-n" ];
	then
		echo ERROR: unexpected amount of arguments
		echo Usage: ./yates_script \<*dot\> \<mtxfile\> \<*.hosts\> \<scrubfile\> \<attack_strength\>
		exit 1
	fi
elif [ $# -gt 7 ] 
then
	echo ERROR: unexpected amount of arguments
	echo Usage: ./yates_script \<*dot\> \<mtxfile\> \<*.hosts\> \<scrubfile\> \<attack_strength\> 
	exit 1
fi

if [ $# -lt 5 ]
then
	echo ERROR: unexpected amount of arguments
	echo Usage: ./yates_script \<*dot\> \<mtxfile\> \<*.hosts\> \<scrubfile\> \<attack_strength\> 
	exit 1
fi

declare -a tops
declare -a nums
num_lines=0
#while read line; do
    #echo $line
    #array=($line)
    #nums+=(${array[0]})
    #tops+=(${array[1]})
    #num_lines=$((num_lines+1))
#done < $5

#testing=$((${nums[0]}+200))
#echo $testing

#Get the folder name
category=${1%.dot}
category=${category##*/}
mtxname=${2%.txt}
mtxname=${mtxname##*/}
echo mtxname
#echo $category
mode="scrub"
attack_strength=$5
attack_increase=$5
attack_length=10
gbps=$((attack_strength/1000000000)).0
mkdir $category"_"$mode
mkdir $category"_"$mode/$gbps"gbps_"$attack_length"duration"
mkdir $category"_"$mode/$gbps"gbps_"$attack_length"duration"/demands
mkdir $category"_"$mode/$gbps"gbps_"$attack_length"duration"/congestions
mkdir $category"_"$mode/$gbps"gbps_"$attack_length"duration"/csv_data
#mkdir $category/$gbps/scrubmtx
n=-1

#This reads line from the main matrix file, and puts each line into another matrix file, each with one line
#This will get concatenated with leftover demand, so demand0.txt leftover demand gets concatenated to demand1.txt
while read line; do
	#echo $line
	n=$((n+1))
	demand="demand$n.txt"
	echo demand$n.txt
	echo $line >> $demand
done < $2


#check if there are too many topologies
echo $num_lines $n
if [ $num_lines -gt $n ]
then 
	echo "ERROR: too many topologies in $5"
	exit 1
fi

x=0
scrubfile=$4
topfile=$1
#echo testing: $((${nums[$x]}-1))
echo here
echo $n
echo Attack strength: $gbps gbps >> $category"_"$mode/$category"_percentages_"$attack_length".txt"
for (( i=0; i<$n; i++ ))
do 
	if [ $((${nums[$x]}-1)) -eq $i ]
	then
	       topfile=${tops[$x]}
	       x=$((x+1))
	       category=${topfile%.dot}
	       category=${category##*/}
	       echo $category
	       #mkdir $category
	       #mkdir $category/demands
	       #mkdir $category/congestions

	fi
	echo $category$i
	#echo $topfile	
	python3 scrubber_simulation.py demand$i.txt $3 $attack_strength $mode $category $scrubfile 
	echo calling yates
	post_scrub_mtx=$category"_"$mode/demand$i"_"post_scrub.txt 
	~/.opam/4.06.0/bin/yates $topfile $post_scrub_mtx $post_scrub_mtx $3 -ecmp
	python3 update_matrix_yates_scrubber.py $post_scrub_mtx $3 ./data/results/$category/EdgeExpCongestionVsIterations.dat ./data/results/$category/paths/ecmp_0 demand$((i+1)).txt
	cp ./data/results/$category/EdgeExpCongestionVsIterations.dat ./$category"_"$mode/$gbps"gbps_"$attack_length"duration"/congestions/congestion$i
	python3 compile_data.py $category"_"$mode/$gbps"gbps_"$attack_length"duration"/congestions/congestion$i $category"_"$mode/$gbps"gbps_"$attack_length"duration"/csv_data/csv_congestion$i.csv
	#attack_strength=$((attack_strength+attack_increase))
	if [ $attack_length -eq $i ]
	then
		attack_strength=0
		echo Attack Ended. >> $category"_"$mode/$category"_percentages_"$attack_length".txt"
	fi
	if [ $attack_strength -eq 0 ]
	then
		tag=$( tail -n 1 $category"_"$mode"/"$category"_percentages_"$attack_length".txt" )
		match="100% of real throughput sent"
		match2="100.0% of real throughput sent"
		echo $tag
		echo $match
		if [ "$tag" = "$match" ] || [ "$tag" = "$match2" ]
		then
			echo The scrubber succesfully defended against attack at time $i >> $category"_"$mode/$category"_percentages_"$attack_length".txt"
			i=$n
		fi

	fi
done

echo "" >> $category"_"$mode/$category"_percentages_"$attack_length".txt"
category=${1%.dot}
category=${category##*/}
for (( i = 0; i<=$n; i++ ))
do
	mv demand$i.txt $category"_"$mode/$gbps"gbps_"$attack_length"duration"/demands
done

echo done

