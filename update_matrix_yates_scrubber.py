"""
Updates the transformation matrix to account for the demand that makes it through, and updates the demand
accordingly.
"""
import sys

#print(f"Number of arguments: {len(sys.argv)}")
#print("argument list: ", str(sys.argv))

def output_matrix(TM, f):
    #f = open("updated_TM.txt", "w")
    for i in range(len(TM)):
        for j in range(len(TM[i])):
            f.write(str(TM[i][j]))
            f.write(" ")
    f.write("\n")

    #f.close()

def get_matrix(f, count): 
    matrix = f.readline()
    matrix = matrix.strip()
    matrix = matrix.split(" ")
    #print("MATRIX: " + str(matrix))
    if(matrix == ['']):
        return False

    #for i in range(len(matrix)):
        #print(matrix[i])

    #turn items to float from str
    for i in range(len(matrix)):
        if matrix[i] != '\n':
            matrix[i] = float(matrix[i])

    #print(matrix)
    TM = []
    for i in range(0, len(matrix), count):
        row = matrix[i:i+count] 
        TM.append(row)

    
    return TM



def calc_matrix(TM):
    #this gets the host to host index that we use to index into TM
    ecmp = open(args[4], "r")
    new_matrix = TM
    #path = "initiate"
    path = ecmp.readline()
    #While not end of file
    while(path != ""):
        paths = []
        #while not end of path list
        while(path!="\n"):
            #If we are at a host-to-host line, find the index
            if (path[0] == "h"):
                path = path.split()
                index = []
                for term in path:
                    if term[0] == "h":
                        index.append(int(term[1:]))
                #print("INDEX: " + str(index))
                row = index[0]
                column = index[1]

                #get it in zero index
                T_SENT = TM[row-1][column-1]
                path = ecmp.readline()

            elif(path[0] == "["):
                #This whole section gets the list of edges from the path to be used in the .dat
                path = path.split(' ')
                counter = 0
                #string magic
                for term in path:
                    string_list = list(term)
                    for i in range(len(term)):
                        if (string_list[i] == ']'):
                            string_list[i] = ''
                        if (string_list[i] == '[' or string_list[i] == ','): 
                            if (i+1 < len(string_list) and (string_list[i+1] == 's' or string_list[i+1] == 'h')):
                                pass
                            else:
                                string_list[i] = ''
                            #print(string_list)
                        path[counter] = "".join(string_list)
                        #print("TERM: " + str(term))
                    #print("PATH: " + str(path))
                    counter+=1
                paths.append(path)
                path = ecmp.readline()
                #ecmp.close()
        
        #We get to this case when we have read all the paths in a given host-to-host connection
        #if(len(paths)>1):
            #print("HERE!")

        dat = open(args[3], "r")
        #find the congestion on a given path, with each edge given it's own congestion, put it into list
        congestion = []
        #print(len(paths))
        for i in range(len(paths)):
            congestion.append([])

            for j in range(len(paths[0])-2):
                dat.seek(0)
                while(True):
                    edges = dat.readline()
                    edges = edges.split()
                    #end of file
                    if (edges == []):
                        break
                    if (paths[i][j] == edges[0]):
                        #print("here")
                        congestion[i].append(float(edges[2]))

        #print("PATH : " + str(paths[0]))
        #print("congestion : " + str(congestion))
        
        #Data sent to receiver
        data = TM[row-1][column-1]

        
        #print(TM)
        loss = 0
        """
        At the end of this loop, T_SENT is the amount of throughput that made it to destination
        So loss of data due to congestion is initial amount - amount that got sent = lost amount
        That then gets appended to the new TM, now showing total data that still needs to be sent
        from one host to another
        """
        throughput = []
        for i in range(len(paths)):
            #Data being sent through this path based on percentage 
            sent = T_SENT * float(paths[i][-1])
            #print("SENT: " + str(len(paths[0])-2))
            for j in range(len(paths[0])-2):
                #print(str(i) + " " +  str(j))
                if congestion[i][j] > 1:
                    sent = (1/congestion[i][j]) * sent
            throughput.append(sent)
        
        T_SENT = 0
        for i in range(len(throughput)):
            #print(throughput[i])
            T_SENT += throughput[i]
        loss = data-T_SENT
        new_matrix[row-1][column-1] = loss

        path = ecmp.readline()
        while(path == "\n"):
            path = ecmp.readline()

    return new_matrix

def concatenate(TM, new_matrix):
    for i in range(len(TM)):
        for j in range(len(TM[i])):
            TM[i][j] += new_matrix[i][j]
    return TM

#first.seek(0)
#output = open("updated_TM_concatenated.txt", "w")

if __name__ == '__main__':
    args = sys.argv

    if (len(sys.argv) != 6):
        print("ERROR: wrong amount of arguments!")
        print("Usage: update_matrix_yates.py <*.txt> <*.hosts> <*.dat> <ecmp> <*.txt>")
        exit()

    #How many hosts are there?
    f = open(args[2], "r")
    count = len(f.readlines())
    #print(count)
    f.close()

    first = open(args[1], "r")
    second = open(args[5], "r")
    #matrices = len(first.readlines())
    TM = get_matrix(first, count)
    #print("MATRIX:")
    #print(TM)
    throughput_to_scrubber = 0
    throughput_to_scrubber_c = 0
    for i in range(1):
        for j in range(len(TM)):
            throughput_to_scrubber += TM[i][len(TM)-1]

        new_matrix = calc_matrix(TM)
       # print(new_matrix)
        for j in range(len(TM)):
            throughput_to_scrubber_c += new_matrix[i][len(TM)-1]

        #print("HERE: ", (throughput_to_scrubber - throughput_to_scrubber_c))
        if (throughput_to_scrubber - throughput_to_scrubber_c) > 1000e9:
            print("GREATER THAN!\n")

        TM = get_matrix(second, count)
        #print(TM)
        second.close()
        if TM != False:
            concatenate(TM, new_matrix)

        second = open(args[5], "w")
        second.seek(0)
        output_matrix(TM, second)

    #print("MATRIX:")
    #print(TM)
    """
    TM = get_matrix(f)
    print(TM)
    print(new_matrix)

    output_matrix(TM, output)
    print(TM)
    """
    first.close()
    second.close()
    #print(new_matrix)











