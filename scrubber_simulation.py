"""
Has 4 modes: SDN, scrub-mtx, scrub, and optical. 
Scrub-mtx: creates a scrubber matrix from a normal matrix to determine what the network looks like with no 
attack traffic
Scrub: takes in a matrix created by scrub-mtx and adds the specified amount of attack throughput. It then determines 
how that attack strength affects the network/scrubber. 
Optical: set up the same as scrubber matrix except there is a specified amount of trusted throughput. 
SDN: Determines the effect of an attack on the network based on the individual scrubber nodes 
"""

import sys
from update_matrix_yates_scrubber import get_matrix, output_matrix

def print_matrix(matrix):
 for i in range(len(matrix)):
     for j in range(len(matrix[i])):
         print(matrix[i][j], end=" ")
     print()

def get_filename(filename):
    filename = filename.split('/')
    filename = filename[-1].split('.')
    filename = filename[0]
    return filename

def find_throughput(matrix, nodes):
        """
        Combines the throughput that all hosts are recieving to find baseline real throughput.
        """
        d = {}
        for i in range(nodes):
            key = "h"+str(i+1)
            d[key] = 0
        for i in range(nodes):
            for j in range(nodes):
               key = "h"+str(j+1)
               d[key] += matrix[i][j]
                                                                                                 
        return d

def find_sent_throughput(matrix, nodes):
        """
        Finds throughput sent by each host.
        This is the amount that will be sent to the scrubber node.
        This is important due to congestion.
        """
        d = {}
        for i in range(nodes):
            key = "h"+str(i+1)
            d[key] = 0
        for i in range(nodes):
            key = "h"+str(i+1)
            for j in range(nodes):
                d[key] += matrix[i][j]
                #print(f"adding {matrix[i][j]} to {key}")

        return d

def check(args):
    if len(args) != 7:
        print("ERROR: incorrect amount of arguments")
        print("USAGE: scrubber_simulation.py <mtx> <hosts> <attack_strength> <mode> <directname> <throughput_input>")
        exit()


def main():
    args = sys.argv
    check(args)
    f = open(args[1], "r")
    hosts = open(args[2], "r")
    attack_strength=float(args[3])
    mode = args[4]
    #1Tbps
    scrubber_throughput = 1000e9
    #real_throughput = 10e9
    temp_str = hosts.readlines()
    nodes = len(temp_str)
    matrix = get_matrix(f, nodes)
    throughput_to_scrubber = 0
    print(f"Attack strength of {attack_strength/1e9} Gbps")
    attack_per_node=0
    dropped_rate = 0
    num_iterations = 0
    mtxname = get_filename(args[1])
    directname = args[5]
    throughput_input = args[6]

    f.close()
    hosts.seek(0)



    #add scrubber node
    #matrix.append([])
    """
    for i in range(len(matrix)):
    matrix[i].append(0.0)
    for j in range(len(matrix[0])-1):
    if i == len(matrix)-1:
    matrix[i].append(0.0)
    """
    if mode == "sdn":
        throughput_f = open(throughput_input, "r")
        real_throughput = float(throughput_f.readline())
        if attack_strength != 0:
            attack_per_node = attack_strength/(nodes)

        #Add attack throughput
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                matrix[i][j]+=attack_per_node / (nodes-1)
                if i == j:
                    matrix[i][j] = 0

        #print_matrix(matrix)
        
        d = find_throughput(matrix, nodes)
        SDN_throughput = 500e9
        #SDN_throughput = 100
        scrub_per_node = SDN_throughput / nodes
        #print(nodes)
        #print(scrub_per_node)
        #print_matrix(matrix)
        #print(d)


        dropped = {}
        dropped_data = {}
        sent_throughput = 0
        #print(d)
        for key in d:
            if d[key] > scrub_per_node:
                dropped[key] = d[key] / scrub_per_node
                dropped_data[key] = (real_throughput / nodes) / dropped[key]
                sent_throughput += dropped_data[key]
            else:
                sent_throughput += d[key]

        #print(dropped)
        #print(dropped_data)
        sent = round(sent_throughput / real_throughput * 100, 2) 
        if sent_throughput >= real_throughput:
            sent = 100
        print(sent, "% of real throughput sent")

        for key in d:
            d[key] -= scrub_per_node

        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                matrix[i][j] -= scrub_per_node / (nodes-1)
                if i == j:
                    matrix[i][j] = 0
                if matrix[i][j] < 0:
                    matrix[i][j] = 0

        #print_matrix(matrix)
        output = open("{}_{}/{}_percentages_10.txt".format(directname, mode, directname), "a")
        output.write(str(sent) + "% of real throughput sent\n")
        output.close()

               


        """
        while(attack):
            attack_iterations+=1
            attack = False
            for key in d:
                d[key] -= scrub_per_node
                if d[key] > scrub_per_node:
                    attack = True
        """

        output = open("{}_{}/{}_post_scrub.txt".format(directname, mode, mtxname), "w")
        #output = open("{}_scrub/{}_{}_post_scrub.txt".format(mtxname, mtxname, attack_strength), "w")
        output_matrix(matrix, output)
        output.close()
        #output2 = open("{}_scrub/{}_{}_scrub_info.txt".format(mtxname, mtxname, attack_strength), "w")

    elif mode == "scrub-mtx":
        received = find_throughput(matrix, nodes-1)
        print(received)
        real_throughput = 0
        if attack_strength != 0:
            attack_per_node = attack_strength/((nodes-1) * (nodes-1))

        print_matrix(matrix)
        #make a node the scrubber node
        #before attack takes no throughput or does no output
        for i in range(len(matrix)):
            matrix[i][len(matrix)-1] = 0.0
            matrix[len(matrix)-1][i] = 0.0

        #add attack throughput
        for i in range(len(matrix)-1):
            for j in range(len(matrix[i])-1):
                 matrix[i][j]+=attack_per_node

        sent_per_node = find_sent_throughput(matrix, nodes-1)
        print(sent_per_node)

        output = open("{}_{}/{}_{}_pre_scrub.txt".format(directname, mode, mtxname, attack_strength/1e9), "w")
        output_matrix(matrix, output)
        output.close()

        throughput_output = open("{}_{}/{}_throughput_{}.txt".format(directname, mode, mtxname, attack_strength/1e9), "w")
        print("output file is: {}_{}/{}_throughput_{}.txt".format(directname, mode, mtxname, attack_strength/1e9))

        """
        if throughput_input == '0':
        print("reading throughput from file")
        throughput = open(throughput_input, "r")
        real_throughput = float(throughput.readline())
        throughput_to_scrubber = float(throughput.readline())

        else:
        """
        for key in received:
            real_throughput += received[key]
            throughput_output.write(str(received[key]) + ",")  
            print("writing: ", received[key])
        
        throughput_output.write("\n")   
        for key in sent_per_node:
            throughput_output.write(str(sent_per_node[key])+",")

        for i in range(len(matrix)-1):
            for j in range(len(matrix[i])-1):
                throughput_to_scrubber += matrix[i][j]
                matrix[i][j] = 0.0

        print(real_throughput / 1e9)

        total_throughput = real_throughput + attack_strength
        throughput_output.write("\n" + str(real_throughput))

        print("Real throughput: ", real_throughput, "Total:  ", total_throughput)


        #output = open("{}_scrub/{}_{}_pre_scrub.txt".format(mtxname, mtxname, attack_strength/1e9), "w")



        print("here:",throughput_to_scrubber)
        #print(throughput_to_scrubber/1e9)
        #divide throughput up between hosts
        count_per_node = throughput_to_scrubber/(nodes-1)
        for i in range(len(matrix)-1):
            key = "h"+str(i+1)
            matrix[i][len(matrix)-1] = sent_per_node[key]
        #output = open("{}_scrub/{}_{}_post_scrub.txt".format(mtxname, mtxname, attack_strength/1e9), "w")
        output = open("{}_{}/{}_post_scrub.txt".format(directname, mode, mtxname), "w")
        print("{}_{}/{}_post_scrub.txt".format(directname, mode, mtxname))

        #output2 = open("{}_scrub/{}_{}_scrub_info.txt".format(mtxname, mtxname, attack_strength/1e9), "w")
        output2 = open("{}_{}/{}_{}_scrub_info.txt".format(directname, mode, mtxname, attack_strength/1e9), "w")
        output2.write(str(throughput_to_scrubber/1e9) + " Gbps of Throughput to scrubber; ")

        percent_real = real_throughput / throughput_to_scrubber
        print(percent_real)
        sent_throughput=0
        print(real_throughput / (nodes-1))
        temp = 0
        dropped_rate = 0
        for i in range(len(matrix)-1):
            #What the scrubber sends back
            #We subtract sent + dropped from total real throughput
            key = "h"+str(i+1)
            if(throughput_to_scrubber > scrubber_throughput):
                dropped_rate = throughput_to_scrubber / scrubber_throughput
                #if (throughput_to_scrubber <= scrubber_throughput):
                matrix[len(matrix)-1][i] = (received[key]) / dropped_rate
                sent_throughput += (received[key]) / dropped_rate
                temp+=(real_throughput/(nodes-1) * percent_real)
            else:
                matrix[len(matrix)-1][i] = received[key]
                sent_throughput += received[key]


        print(f"Scrubber overloaded by {dropped_rate}")
        output2.write(f"Scrubber overloaded by {dropped_rate}\n")
        throughput_to_scrubber -= scrubber_throughput

        leftover_throughput = real_throughput - temp
        print("Real throughput after first cycle: ", leftover_throughput)

        #sent_throughput = round(sent_throughput, 0)
        print(sent_throughput)
        print(real_throughput)
        output_matrix(matrix, output)
        print_matrix(matrix)
        #real_throughput = real_throughput - sent_throughput
        print((sent_throughput / real_throughput) * 100, "%")

        percent_real = leftover_throughput / total_throughput
        output2.write(f"\nIterations to defend: {num_iterations}\n")
        output.close()
        output2.close()

    elif mode == "scrub":
        #print_matrix(matrix)
        throughput_f = open(throughput_input, "r")
        data_r = throughput_f.readline()
        data_s = throughput_f.readline()
        real_throughput = float(throughput_f.readline())
        #print(real_throughput)

        #get total throughput to scrubber
        for i in range(nodes-1):
            matrix[i][nodes-1] += (attack_strength / (nodes-1))
            throughput_to_scrubber += matrix[i][nodes-1]


        #subtract scrubber capabilities
        throughput_to_scrubber -= scrubber_throughput
        if throughput_to_scrubber <= 0:
            throughput_to_scrubber = 0

        #remake the matrix throughput_to_scrubber
        for i in range(nodes-1):
            #print(matrix[i][nodes-1], scrubber_throughput/(nodes-1))
            if ((scrubber_throughput / (nodes-1)) < matrix[i][nodes-1]):
                matrix[i][nodes-1] -= (scrubber_throughput / (nodes-1))

        data_to_scrub = data_s.split(",")
        data_to_scrub.pop()
        out_data = data_r.split(",")
        out_data.pop()

        for i in range (len(data_to_scrub)):
            data_to_scrub[i] = float(data_to_scrub[i])
            out_data[i] = float(out_data[i])
        #print(data_to_scrub)

        sent_throughput=0
        #print(real_throughput / (nodes-1))
        temp = 0
        dropped_rate = 0
        #print(throughput_to_scrubber)
        #print(real_throughput)

        for i in range(len(matrix)-1):
            #What the scrubber sends back based on how overloaded it is
            #We subtract sent + dropped from total real throughput
            if(throughput_to_scrubber > scrubber_throughput):
                dropped_rate = throughput_to_scrubber / scrubber_throughput
                #if (throughput_to_scrubber <= scrubber_throughput):
                matrix[len(matrix)-1][i] = (out_data[i])/dropped_rate
                sent_throughput += (out_data[i]) / dropped_rate
            else:
                #print_matrix(matrix)
                #print(out_data)
                matrix[len(matrix)-1][i] = out_data[i]
                sent_throughput += out_data[i]

            """
            elif (throughput_to_scrubber < real_throughput):
            matrix[len(matrix)-1][i] = matrix[i][len(matrix)-1]
            sent_throughput += matrix[i][len(matrix)-1]
            """
        #print(throughput_to_scrubber)
        print(f"Scrubber overloaded by {dropped_rate}")
        print("output file : {}_{}/{}_post_scrub.txt".format(directname, mode, mtxname))
        output = open("{}_{}/{}_post_scrub.txt".format(directname, mode, mtxname), "w")
        output_matrix(matrix, output)
        #print_matrix(matrix)
        output.close()

        #print("Total sent throughput: ", sent_throughput)
        if (throughput_to_scrubber < real_throughput):
            sent = 100
        else:
            sent = round((sent_throughput / real_throughput) * 100, 2)
        print(sent, "% of real throughput sent")
        output = open("{}_{}/{}_percentages_10.txt".format(directname, mode, directname), "a")
        #if attack_strength > 0:
            #output.write("Attack strength: {} gbps\n".format(attack_strength / 1e9))
        output.write(str(sent) + "% of real throughput sent\n")
        output.close()

    #optical defense
    else: 
        percent = mode.split("-")
        percent_trusted = float(percent[-1]) / 100
        #print(percent_trusted)
        #print_matrix(matrix)
        throughput_f = open(throughput_input, "r")
        data_r = throughput_f.readline()
        data_s = throughput_f.readline()
        real_throughput = float(throughput_f.readline())
        trusted_throughput = real_throughput * percent_trusted
        benign_throughput = real_throughput - trusted_throughput
        percent_benign = 1 - percent_trusted
        #print(real_throughput)

        #get total throughput to scrubber
        for i in range(nodes-1):
            matrix[i][nodes-1] += (attack_strength / (nodes-1))
            throughput_to_scrubber += matrix[i][nodes-1]


        #subtract scrubber capabilities
        throughput_to_scrubber -= scrubber_throughput
        if throughput_to_scrubber <= 0:
            throughput_to_scrubber = 0

        #remake the matrix throughput_to_scrubber
        for i in range(nodes-1):
            #print(matrix[i][nodes-1], scrubber_throughput/(nodes-1))
            if ((scrubber_throughput / (nodes-1)) < matrix[i][nodes-1]):
                matrix[i][nodes-1] -= (scrubber_throughput / (nodes-1))

        data_to_scrub = data_s.split(",")
        data_to_scrub.pop()
        out_data = data_r.split(",")
        out_data.pop()

        for i in range (len(data_to_scrub)):
            data_to_scrub[i] = float(data_to_scrub[i])
            out_data[i] = float(out_data[i])
        #print(data_to_scrub)

        sent_throughput=0
        #print(real_throughput / (nodes-1))
        temp = 0
        dropped_rate = 0
        #print(throughput_to_scrubber)
        #print(real_throughput)
        out_data_benign = []
        out_data_trusted = []
        for i in range(len(out_data)):
            out_data_benign.append(out_data[i] * percent_benign)
            out_data_trusted.append(out_data[i] * percent_trusted)

        #print(out_data)
        #print(out_data_benign)
        #print(out_data_trusted)
        print_matrix(matrix)

        for i in range(len(matrix)-1):
            #What the scrubber sends back based on how overloaded it is
            #We subtract sent + dropped from total real throughput
            if(throughput_to_scrubber > scrubber_throughput):
                dropped_rate = throughput_to_scrubber / scrubber_throughput
                #if (throughput_to_scrubber <= scrubber_throughput):
                matrix[len(matrix)-1][i] = out_data_trusted[i] + ((out_data_benign[i])/dropped_rate)
                sent_throughput += out_data_trusted[i] + ((out_data_benign[i]) / dropped_rate)
            else:
                #print_matrix(matrix)
                #print(out_data)
                matrix[len(matrix)-1][i] = out_data[i]
                sent_throughput += out_data[i]

            """
            elif (throughput_to_scrubber < real_throughput):
            matrix[len(matrix)-1][i] = matrix[i][len(matrix)-1]
            sent_throughput += matrix[i][len(matrix)-1]
            """
        #print("REAL THROUGHPUT ", real_throughput)
        #print("SENT THROUGHPUT ", sent_throughput)
        #print(throughput_to_scrubber)
        print(f"Scrubber overloaded by {dropped_rate}")
        print("output file : {}_{}/{}_post_scrub.txt".format(directname, mode, mtxname))
        output = open("{}_{}/{}_post_scrub.txt".format(directname, mode, mtxname), "w")
        output_matrix(matrix, output)
        #print_matrix(matrix)
        output.close()

        #print("Total sent throughput: ", sent_throughput)
        #print("THROUGH TO SCRUB ", throughput_to_scrubber)
        if (throughput_to_scrubber < real_throughput):
            sent = 100
        else:
            sent = round((sent_throughput / real_throughput) * 100, 2)
        print(sent, "% of real throughput sent")
        output = open("{}_{}/{}_percentages_10.txt".format(directname, mode, directname), "a")
        #if attack_strength > 0:
            #output.write("Attack strength: {} gbps\n".format(attack_strength / 1e9))
        output.write(str(sent) + "% of real throughput sent\n")
        output.close()

main()






