"""
Increases demand equally across all host to host connections by a certain multiplier
Does not create new output file, increases matrix in place
Usage: ./increase_demand <mtx> <hosts> "increase/decrease" <multiplier>
Example: python3 increase_demand NSF.txt NSF.hosts increase 10
This will increase every number in the matrix by a factor of 10 
1 1 1 1      10 10 10 10
1 1 1 1 ---> 10 10 10 10    
1 1 1 1      10 10 10 10
1 1 1 1      10 10 10 10
Author: Max Hopkins
"""
from update_matrix_yates_scrubber import get_matrix, output_matrix
import sys


args = sys.argv
if len(args) != 5:
    print("Usage: ./increase_demand mtx hosts increase/decrease multiplier")
    exit(1)

multiplier = float(args[4])
f = open(args[1], 'r')
hosts = open(args[2], 'r')

nodes = len(hosts.readlines()) 

matrix = get_matrix(f, nodes)

if args[3] == "increase":
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            matrix[i][j] = matrix[i][j] * multiplier

elif args[3] == "decrease":
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            matrix[i][j] = matrix[i][j] / multiplier
f.close()
throughput_to_scrubber = 0
for i in range(len(matrix)):
    for j in range(len(matrix)):
        if i != j:
            throughput_to_scrubber += matrix[i][j]
        
mtxname = args[1].split('/')
mtxname = mtxname[-1].split('.')
mtxname = mtxname[0]
print(mtxname)
print(throughput_to_scrubber)
print("Throughput to scrubber: ", throughput_to_scrubber / 1e9, " gbps")
output = open("{}_throughput.txt".format(mtxname), "w")
output.write(str(throughput_to_scrubber))


f = open(args[1], 'w')
output_matrix(matrix, f)

f.close()
hosts.close()
