"""
This program extracts data from the defense data file for the time to end attack and the amount of throughput that
was able to make it through. It then puts this data into a .csv file in order to be put into graphs
"""

import sys

args = sys.argv

if len(sys.argv) != 3:
    print("ERROR: Need file name")
    exit()

filename = args[1]
through = args[2]
directname = filename.split("/")
directname = directname[0]
f = open(filename, "r")
through_file = open(through, "r")

line = f.readline()
output = open("{}/defense_retaliation_data.csv".format(directname), "w")
output2 = open("{}/defense_throughput_data.csv".format(directname), "w")
gbps = 0
total = 0
count = 0

throughput = through_file.readline()
throughput = throughput.split(",")
while len(throughput) > 2:
    throughput = through_file.readline()
    throughput = throughput.split(",")
throughput = float(throughput[0])
total_throughput = throughput * 61
        
while(line != ""):
    if line[0] == "T":
        line = line.split()
        time = int(line[-1])
        gbps += 100
        output.write("{},{}\n".format(gbps, time))

    elif line[0] != "A" and line != "\n":
        line = line.split()
        line = line[0]
        line = line.split("%")
        line = line[0]
        total += throughput * (float(line) / 100)

    if line == "\n":
        if time < 60:
            total += throughput * (60-time)
        #print(60-time)
        percentage = total / total_throughput 
        
        output2.write("{},{}\n".format(gbps, percentage))
        total = 0
    #print(line)

    line = f.readline()


f.close()
output.close()
