"""
This will take yates congestion data and create a .csv file with all the congestions.
The program will sort the congestion data from lowest to highest
and also create a complimentary CDF. This can be opened
in excel to create graphs.
Usage: ./compile_data.py <congestionfile> <outputfile>
Congestionfile: A yates generated congestion file
outputfile: custom output file to store the data
Example: python3 compile_data.py NSF_Waves_initial_4/EdgeExpCongestionVsIterations.dat nsf_initital.csv
Author: Max Hopkins
"""
import sys

def main():
    args = sys.argv
    f = open(args[1], "r")
    output = open(args[2], "w")
    congestion = f.readline()
    number = 0
    congestion_list = []
    output.write("Congestion,CDF\n")
    while(congestion != "\n"):
        congestion = congestion.strip()
        if congestion[0] == "(":
            congestion = congestion.split(':')
            number = float(congestion[1])
            congestion_list.append(number)
            congestion = f.readline()
            """
            if congestion == "\n":
                output.write(str(number))
            else:
                output.write(str(number) + ",") 
            """
        else:
            congestion = f.readline()

    congestion_list.sort()
    count = len(congestion_list)

    for i in range(count):
        output.write(str(congestion_list[i]) + ",")
        output.write(str((i+1)/(count))+"\n")
    """
        else:
            output.write(str(congestion_list[i]))
    output.write("\n")
    for i in range(count-1):
        if(i != count-2):
            output.write(str((i+1)/(count-1))+ ",")
        else:
            output.write(str((i+1)/(count-1)))
    """     


    print(count)
    output.close()
    f.close()

main()
