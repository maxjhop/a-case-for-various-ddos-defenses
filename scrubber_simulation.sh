#!/bin/bash
# This script will call the yates_srub.sh over the course of 50 iterations in order to gather data for attack strengths
# ranging from 100 Gbps to 5000 Gbps
# Usage: ./scrubber_simulation.sh <topo> <mtx> <hosts> <scrubfile> <attack_strength> 

topo=$1
mtxfile=$2
hosts=$3
throughput=$4
attack_strength=$5
attack_increase=$5
mtxname=${mtxfile%.txt}
mtxname=${mtxname##*/}
toponame=${topo%.dot}
toponame=${toponame##*/}
#mode=$5
echo $toponame $mtxname

#mkdir $mtxname"_"$mode
#mkdir $mtxname"_"$mode/csv_data

if [ $# -ne 5 ]
then
	echo ERROR: wrong amount of arguments
	echo Usage: ./scrubber_simulation.sh topo mtx hosts throughput attack_strength
	exit 1
fi

for (( i=0; i<50; i++ ))
do
	gbps=$((attack_strength/1000000000)).0
	./yates_scrub.sh $topo $mtxfile $hosts $throughput $attack_strength  
	#gbps=$((attack_strength)).0
	#python3 scrubber_simulation.py $mtxfile $hosts $attack_strength $mode 
	#yates $topo $mtxname"_"$mode/$mtxname"_"$gbps"_post_scrub.txt" $mtxname"_"$mode/$mtxname"_"$gbps"_post_scrub.txt" $hosts -ecmp 
	#echo calling compile data
	#python3 compile_data.py data/results/$toponame/EdgeExpCongestionVsIterations.dat $mtxname"_"$mode/csv_data/$mtxname"_"$gbps"_congestion.csv"
	attack_strength=$((attack_strength+attack_increase))

done
